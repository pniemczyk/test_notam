require 'rails_helper'

RSpec.describe NotamParser do
  subject { described_class[notam_one] }

  let(:notam_one) { load_payload('notams/one.txt') }
  let(:notam_two) { load_payload('notams/two.txt') }

  context 'returns Notham instance with' do
    it '#source' do
      expect(subject.source).to eq(notam_one)
    end

    it '#icao' do
      expect(subject.icao).to eq('ESGJ')
    end

    context '#days' do
      it 'is a Notam::DaysInfo' do
        expect(subject.days).to be_a(Notam::DaysInfo)
      end

      it 'contain all days with data for notam_one' do
        expect(subject.days.mon).to eq('05001830')
        expect(subject.days.tue).to eq('05001830')
        expect(subject.days.wed).to eq('05001830')
        expect(subject.days.thu).to eq('05002130')
        expect(subject.days.fri).to eq('07302100')
        expect(subject.days.sat).to eq('06300730, 19002100')
        expect(subject.days.sun).to eq('CLOSE')
      end

      it 'contain all days with data for notam_two' do
        tested_notam = described_class[notam_two]
        expect(tested_notam.days.mon).to eq('05002000')
        expect(tested_notam.days.tue).to eq('0500-2100')
        expect(tested_notam.days.wed).to eq('0500-2100')
        expect(tested_notam.days.thu).to eq('0500-2100')
        expect(tested_notam.days.fri).to eq('0545-2100')
        expect(tested_notam.days.sat).to eq('0630-0730, 1900-2100')
        expect(tested_notam.days.sun).to eq('CLOSED')
      end
    end
  end
end
