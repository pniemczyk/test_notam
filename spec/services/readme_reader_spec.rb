require 'rails_helper'

RSpec.describe ReadmeReader do
  subject { described_class }
  let(:filename) { 'README.md' }
  let(:relative_file_path) { [Rails.root.to_s, filename].join('/') }
  it 'returns html form markdown file' do
    expect(File).to receive(:read).with(relative_file_path).and_return("TEST\n---")
    expect(subject[filename]).to include('TEST</h2>')
  end

  it 'raise not_found error' do
    expect { subject['unknown_readme_file'] }.to raise_error(ReadmeReader::NotFound)
  end
end
