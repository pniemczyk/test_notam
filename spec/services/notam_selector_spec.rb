require 'rails_helper'

RSpec.describe NotamSelector do
  subject { described_class }
  let(:file) { Tempfile.new('test') }

  let(:invalid_notam) { load_payload('notams/invalid.txt') }
  let(:valid_notam) { load_payload('notams/one.txt') }

  before do
    file.write(valid_notam)
    file.write("\n")
    file.write(invalid_notam)
    file.write("\n")
    file.write(valid_notam)
    file.rewind
  end

  it 'returns only valid notams' do
    notams = subject[file]
    expect(notams.count).to eq(2)
    expect(notams.first).to be_a(Notam)
  end
end
