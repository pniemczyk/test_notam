require 'rails_helper'

RSpec.describe Notam do
  subject { described_class.new }

  it '#source' do
    expect(subject).to respond_to(:source)
  end

  it '#icao' do
    expect(subject).to respond_to(:icao)
  end

  it '#days' do
    expect(subject).to respond_to(:days)
  end
end

RSpec.describe Notam::DaysInfo do
  subject { described_class.new }

  [:mon, :tue, :wed, :thu, :fri, :sat, :sun].each do |attr|
    it "##{attr}" do
      expect(subject).to respond_to(attr)
    end
  end

  it '#all' do
    expect(subject.all).to be_a(Hash)
    expect(subject.all.keys).to include(:mon, :tue, :wed, :thu, :fri, :sat, :sun)
  end
end
