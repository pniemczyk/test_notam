require 'rails_helper'

RSpec.describe 'Upload and display notams', type: :request do
  let(:file) { File.new("#{Rails.root}/spec/fixtures/notams/one.txt") }
  let(:uploaded_file) { Rack::Test::UploadedFile.new(file, 'text/plain') }

  it 'Redirect after uplad successed' do
    post upload_notams_path, source: uploaded_file
    expect(response).to have_http_status(200)
    expect(response.body).to match(/ESGJ/)
  end
end
