require 'rails_helper'

RSpec.describe 'welcome/index.html.haml', type: :view do
  it 'displays link to new notams' do
    view.class.send(:attr_reader, :source)
    assign(:source, 'some_text')

    render
    expect(rendered).to match /Start/
  end
end
