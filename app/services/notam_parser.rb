class NotamParser < MethodStruct.new(:source, method_name: :parse)
  ICAO_REGEX        = /A[\)](\s\w*|\w*)/
  DESCRIPTION_REGEX = /E[\)](.*)/
  DAYNAMES          = %w(MON TUE WED THU FRI SAT SUN)
  DAYS_INFO         = /(?<day_from>#{DAYNAMES.join('|')})(?<day_to>.{3}\s|-\D{3}\s|\s)(?<value>\w*[-]\w*,\s\w*[-]\w*|\w*,\s\w*|\w*[-]\w*|\w*)/
  def parse
    Notam.new(
      source: source,
      icao: icao,
      days: days
    )
  end

  private

  def icao
    extract_value(ICAO_REGEX)
  end

  def extract_value(regex)
    source.scan(regex).flatten.first.to_s.strip
  end

  def details
    @details ||= extract_value(DESCRIPTION_REGEX)
  end

  def days_info
    @days_info ||= details.scan(DAYS_INFO)
  end

  def days
    data = {}
    days_info.each do |day_data|
      day_from, day_to, value = day_data
      days_from_to(day_from.strip, day_to.delete('-').strip).each do |day_name|
        data[day_name.downcase.to_sym] = value
      end
    end
    data
  end

  def days_from_to(day_from, day_to)
    return [day_from] if day_to.blank?
    DAYNAMES[DAYNAMES.index(day_from)..DAYNAMES.index(day_to)]
  end
end
