class ReadmeReader < MethodStruct.new(:relative_file_path, method_name: :show)
  NotFound = Class.new(StandardError)
  def show
    Markdown.new(file).to_html
  rescue Errno::ENOENT
    raise NotFound, file_path
  end

  private

  def file
    File.read(file_path)
  end

  def file_path
    [Rails.root.to_s, relative_file_path].join('/')
  end
end
