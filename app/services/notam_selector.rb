class NotamSelector < MethodStruct.new(:file, method_name: :select)
  VALID_INFORMATION = %r{E\)?\sAERODROME\sHOURS\sOF\sOPS\/SERVICE}
  def select
    file.open.each(sep = "\n\n") do |notam|
      add_notam(notam) if notam.present? && valid?(notam)
    end
    valid_notams
  end

  private

  def valid?(text)
    text =~ VALID_INFORMATION
  end

  def add_notam(text)
    valid_notams.push(NotamParser[text])
  end

  def valid_notams
    @valid_notams ||= []
  end
end
