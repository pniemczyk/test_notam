class WelcomeController < ApplicationController
  def index
    render :index, locals: { source: readme_source }
  end

  private

  def readme_source
    @readme_source ||= ReadmeReader['README.md']
  rescue ReadmeReader::NotFound
    'Not ready yeat'
  end
end
