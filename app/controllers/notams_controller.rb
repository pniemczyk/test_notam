class NotamsController < ApplicationController
  IncorrectFileType = Class.new(StandardError)
  rescue_from ActionController::ParameterMissing, IncorrectFileType, with: :wrong_or_missing_file
  def index
    render :index, locals: { notams: [] }
  end

  def new; end

  def upload
    validate_file_content_type!
    notams = NotamSelector[file_with_notams]
    render :index, locals: { notams: notams }
  end

  private

  def validate_file_content_type!
    if file_with_notams.content_type != 'text/plain'
      raise IncorrectFileType, "#{file_with_notams.content_type} content type is incorrect"
    end
  end

  def wrong_or_missing_file(ex)
    flash[:error] = ex.message
    redirect_to :root
  end

  def file_with_notams
    @file_with_notams ||= params.require(:source)
  end
end
