class Notam
  class DaysInfo
    include ActiveModel::Model
    ATTRIBUTES = [:mon, :tue, :wed, :thu, :fri, :sat, :sun]

    attr_accessor(*ATTRIBUTES)

    def all
      ATTRIBUTES.each_with_object({}) do |attr, hash|
        hash[attr] = public_send(attr)
      end
    end
  end

  include ActiveModel::Model
  attr_accessor :source, :icao

  def days
    @days ||= DaysInfo.new
  end

  def days=(value)
    @days = DaysInfo.new(value)
  end
end
